# BeatFetcher

Useful script for downloading latest song from https://bsaber.com/songs by genres

# How to

```
chmod +x ./download.sh
./download.sh fetch             # If you want to fetch
./download.sh clean             # If you want to clean the dowload folder
./download.sh cat1 cat2 cat3    # If you want to fetch categories
```
