BASE_URL="https://bsaber.com/songs/top/?time=all&genre="
declare -a Genres=("breakcore" "classical" "comedy" "death-metal" "disco" "drum-and-bass" "edm" "future-bass" "video-game")

args=("$@")

function clean {
    for genre in $(ls -d */); do
        rm -rf $genre/*
        mkdir -p $genre
    done
}

function list {
    fetch_genre $args
}

function fetch {
    fetch_genre ${Genres[@]}
}

function fetch_genre {
    for genre in $@; do
        echo "$genre"
        rm -rf $genre/*
        mkdir -p $genre
        wget $BASE_URL$genre -O $genre/source.html
        cat $genre/source.html | grep -E '<a class="action post-icon bsaber-tooltip -download-zip' > $genre/links.txt
        i=0
        grep -Po '(?<=href=")[^"]*' $genre/links.txt | while read -r line ; do 
            wget $line -O $genre/$i
            unzip $genre/$i -d $genre/"$(date +%s)"
            rm $genre/$i
            i=$(expr $i + 1)
            sleep 1
        done
        rm $genre/links.txt
        rm $genre/source.html
    done
}

if [ "${args[0]}" = "clean" ]; then
    clean
elif [ "${args[0]}" = "fetch" ]; then
    fetch
else
    list
fi